package com.jun.cloud.controller;

import com.jun.cloud.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private RestTemplate restTemplate;
    @GetMapping("allstudent")
    public List<Student> selectAll(){
        return restTemplate.getForObject("http://STUDENT-PROVIDER/studentall", List.class);
    }
    @GetMapping("onestudent")
    public Student selectOne(){
        return restTemplate.getForObject("http://STUDENT-PROVIDER/studentone", Student.class);
    }
}
