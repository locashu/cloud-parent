package com.jun.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application7084 {
    public static void main(String[] args) {
        SpringApplication.run(Application7084.class, args);
    }
}
