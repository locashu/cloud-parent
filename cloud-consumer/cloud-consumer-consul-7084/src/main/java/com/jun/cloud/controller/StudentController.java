package com.jun.cloud.controller;

import com.jun.cloud.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private LoadBalancerClient client;
    @GetMapping("allstudent")
    public List<Student> selectAll(){
        ServiceInstance serviceInstance = client.choose("student-provider-8085");
        String host=serviceInstance.getHost();
        System.out.println(host);
        int port = serviceInstance.getPort();
        System.out.println(port);
        //String serviceId = serviceInstance.getServiceId();
        return restTemplate.getForObject("http://"+host+":"+port+"/studentall", List.class);
    }
    @GetMapping("onestudent")
    public Student selectOne(){
        ServiceInstance serviceInstance = client.choose("student-provider-8085");
        String host=serviceInstance.getHost();
        int port = serviceInstance.getPort();
        //String serviceId = serviceInstance.getServiceId();
        return restTemplate.getForObject("http://"+host+":"+port+"/studentone", Student.class);
    }
}
