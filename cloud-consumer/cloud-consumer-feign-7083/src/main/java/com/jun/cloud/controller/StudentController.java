package com.jun.cloud.controller;

import com.jun.cloud.api.IStudentService;
import com.jun.cloud.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private IStudentService studentService;

    @RequestMapping("/studentall")
    public List<Student> selectAll() {
        return studentService.selectAll();
    }
    @RequestMapping("/studentone")
    public Student selectOne(){
        return studentService.selectOne();
    }
}
