package com.jun.cloud.controller;

import com.jun.cloud.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private RestTemplate restTemplate;

    private static final String APPLICATION_NAME="http://STUDENT-PROVIDER";
    @RequestMapping("studentall")
    public List<Student> selectAll(){
        return restTemplate.getForObject(APPLICATION_NAME+"/studentall", List.class);
    }
    @RequestMapping("studentone")
    public Student selectOne() {
        return restTemplate.getForObject(APPLICATION_NAME + "/studentone", Student.class);
    }
}
