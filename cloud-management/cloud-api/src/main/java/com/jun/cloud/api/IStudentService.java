package com.jun.cloud.api;

import com.jun.cloud.model.Student;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@FeignClient(value ="student-provider",fallbackFactory =StudentFallBackFactory.class)
public interface IStudentService {
    @RequestMapping("/studentall")
    List<Student> selectAll();
    @RequestMapping("/studentone")
    Student selectOne();
}
