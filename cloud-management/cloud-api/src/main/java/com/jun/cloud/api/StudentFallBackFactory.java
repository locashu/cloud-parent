package com.jun.cloud.api;

import com.jun.cloud.model.Student;
import feign.hystrix.FallbackFactory;

import java.util.ArrayList;
import java.util.List;

public class StudentFallBackFactory implements FallbackFactory<IStudentService> {
    public IStudentService create(Throwable throwable) {
        return new IStudentService() {
            public List<Student> selectAll() {
                List<Student> studentList=new ArrayList<Student>();
                Student student = new Student();
                student.setId(1000L);
                student.setName("无法降级");
                student.setAge(22);
                student.setSex("男");
                studentList.add(student);
                return studentList;
            }

            public Student selectOne() {
                return null;
            }
        };
    }
}
