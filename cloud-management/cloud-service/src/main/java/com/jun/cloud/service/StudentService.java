package com.jun.cloud.service;

import com.jun.cloud.mapper.StudentMapper;
import com.jun.cloud.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentMapper studentMapper;
    public Student selectByPrimaryKey(long id){
        return studentMapper.selectByPrimaryKey(id);
    }
    public List<Student> selectAll(){
        return studentMapper.selectAll();
    }
}
