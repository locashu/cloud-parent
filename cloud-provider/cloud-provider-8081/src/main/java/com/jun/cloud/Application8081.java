package com.jun.cloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.jun.cloud.mapper")
public class Application8081 {
    public static void main(String[] args) {
        SpringApplication.run(Application8081.class, args);
    }
}
