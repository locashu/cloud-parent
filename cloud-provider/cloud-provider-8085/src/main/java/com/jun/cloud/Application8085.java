package com.jun.cloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@MapperScan("com.jun.cloud.mapper")
@EnableDiscoveryClient
public class Application8085 {
    public static void main(String[] args) {
        SpringApplication.run(Application8085.class,args);
    }
}
