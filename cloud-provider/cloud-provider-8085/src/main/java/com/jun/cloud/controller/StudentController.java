package com.jun.cloud.controller;

import com.jun.cloud.model.Student;
import com.jun.cloud.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private StudentService studentService;
    @RequestMapping("/studentone")
    public Student selectById(){
        return studentService.selectByPrimaryKey(4L);
    }
    @RequestMapping("/studentall")
    public List<Student> selectAll(){
        return studentService.selectAll();
    }
}
