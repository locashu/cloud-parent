package com.jun.cloud.controller;

import com.jun.cloud.model.Student;
import com.jun.cloud.service.StudentService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class StudentController {
    @Autowired
    private StudentService studentService;
    @RequestMapping("/hystrix")
    @HystrixCommand(fallbackMethod = "fallbackStudent")
    public List<Student> selectAll() throws Exception{
        List<Student> studentList = studentService.selectAll();
        throw new RuntimeException("故意抛出异常");
    }

    public List<Student> fallbackStudent(){
        List<Student> students = new ArrayList<Student>();
        Student student = new Student();
        student.setId(2000L);
        student.setName("回调方法");
        student.setAge(33);
        student.setSex("女");
        students.add(student);
        return students;
    }
}
